#include <string>
#include <iostream>

int main()
{
	std::string source{ "I am alive!" };

	std::cout << "String itself: " << source << std::endl;
	std::cout << "String length: " << source.length() << std::endl;
	std::cout << "First symbol: " << source.at(0) << std::endl;
	std::cout << "Last symbol: " << source.back() << std::endl;
}